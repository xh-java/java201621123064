package test1;

public class Employee implements Cloneable {
	private String name;
	private int age;
	public Employee(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}
	public Employee clone() throws CloneNotSupportedException {
		Employee cloned=(Employee)super.clone();
		return cloned;
	}
	
}
