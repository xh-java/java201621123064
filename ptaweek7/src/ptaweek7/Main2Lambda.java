package ptaweek7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable3{
	private String name;
	private int age;
	public PersonSortable3(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public String toString(){
		return this.name+"-"+this.age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}
}

public class Main2Lambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int n=Integer.parseInt(sc.nextLine());
		PersonSortable2[] person=new PersonSortable2[n];
		for(int i=0;i<n;i++){
			PersonSortable2 a=new PersonSortable2(sc.next(),sc.nextInt());
			person[i]=a;
		}
		Arrays.sort(person, (o1,o2)->o1.getName().compareTo(o2.getName()));
		System.out.println("NameComparator:sort");
		for(int i=0;i<n;i++){
	    	System.out.println(person[i]);
	    }
		Arrays.sort(person, (o1,o2)->o1.getAge()-o2.getAge());
		System.out.println("AgeComparator:sort");
		for(int i=0;i<n;i++){
	    	System.out.println(person[i]);
	    }
	}

}
