package Shopping;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

class Product{
	private int priductId;
	private String name;
	private double price;
	private int amount;
	
	public Product(int priductId, String name, double price) {
		this.priductId = priductId;
		this.name = name;
		this.price = price;
	}

	public Product(int priductId, String name, double price, int amount) {
		this.priductId = priductId;
		this.name = name;
		this.price = price;
		this.amount = amount;
	}

	public int getPriductId() {
		return priductId;
	}

	public void setPriductId(int priductId) {
		this.priductId = priductId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return priductId+" "+name+" "+price;
	}
	
}

class Fruit extends Product{

	public Fruit(int priductId, String name, double price) {
		super(priductId, name, price);
	}
	
}
class Snake extends Product{

	public Snake(int priductId, String name, double price) {
		super(priductId, name, price);
	}
	
}

class ShoppingCartListImpl implements ProductDao{
	private static ArrayList<Product> arrayList=new ArrayList<>();
	
	public boolean Add(Product p) {
		arrayList.add(p);
		return true;
	}
	
	public void Show() {
		for(int i=0;i<arrayList.size();i++)
			System.out.println(arrayList.get(i).getPriductId()+" "+arrayList.get(i).getName()+" "+arrayList.get(i).getPrice()+" "+arrayList.get(i).getAmount());
	}
	public void Remove(int i) {
		arrayList.remove(arrayList.get(i-1));
	}
	public void ChangeAmount(int i,int Amount) {
		arrayList.get(i-1).setAmount(Amount);
	}
	public void Delete() {
		for(int i=0;i<arrayList.size();i++)
			arrayList.remove(i);
	}
	public String Sum() {
		double sum=0.0;
		DecimalFormat total=new DecimalFormat("#.##");
		for(int i=0;i<arrayList.size();i++)
			sum+=arrayList.get(i).getPrice()*arrayList.get(i).getAmount();
		return total.format(sum);
	}
	
}

class ShoppingCartArrayImpl implements ProductDao{  //谢晗 201621123064
	private Product[] product;
	private int size;
	
	public ShoppingCartArrayImpl(int n) {
		this.size=n;
		product=new Product[n];
	}

	public boolean Add(Product p) {
		for(int i=0;i<product.length;i++)
			if(product[i]==null) {
				product[i]=p;
				break;
			}
		return true;
	}

	public void Show() {
		for(int i=0;i<product.length;i++)
			if(product[i]==null) {
				break;
			}
			else
			System.out.println(product[i].getPriductId()+" "+product[i].getName()+" "+product[i].getPrice()+" "+product[i].getAmount());
	}

	public void Remove(int i) {
		for(int j=i-1;j<product.length-1;j++) {
			product[j]=product[j+1];
		}
	}

	public void ChangeAmount(int i, int Amount) {
		product[i-1].setAmount(Amount);
	}

	public void Delete() {
		for(int i=0;i<product.length;i++)
			product[i]=null;
	}

	public String Sum() {
		double sum=0.0;
		DecimalFormat total=new DecimalFormat("#.##");
		for(int i=0;i<product.length;i++)
			if(product[i]!=null)
			sum+=product[i].getPrice()*product[i].getAmount();
			else break;
		return total.format(sum);
	}
	
}

public class Shopping {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Product[] products=new Product[5];
		products[0]=new Fruit(101,"    苹果             ",25.78);
		products[1]=new Product(102,"  抹茶蛋糕          ",54.90);
		products[2]=new Snake(103,"  三只松鼠          ",109.23);
		products[3]=new Product(104,"  百草味              ",102.3);
		products[4]=new Product(105,"  良品铺子          ",56.23);
		System.out.println("            ^   ^");
		System.out.println("               欢迎来到吃货世界");
		System.out.println("      编号     食物名称        单价/元");
		for(int i=0;i<5;i++)
			System.out.println("   "+products[i]);
		//ShoppingCartListImpl cart=new ShoppingCartListImpl();
		System.out.println("   ------------------------");
		System.out.println("请问，小主需要点什么？(输入-1结束此次购物)");
		ShoppingCartArrayImpl cart=new ShoppingCartArrayImpl(4);
		int flage=1;
		while(flage!=0) {
			int a=in.nextInt();
			if(a==-1) {
				flage=0;
			}
			else {
			Product product=new Product(a, in.next(), in.nextDouble(), in.nextInt());
			cart.Add(product);
			}
		}
		System.out.println("         |***************************|");
		System.out.println("         |      1.   结算                                 |"); 
		System.out.println("         |      2.   删除商品                       |");
		System.out.println("         |      3.   更改商品件数             |");
		System.out.println("         |      4.   清空购物车                  |");
		System.out.println("         |      5.   退出系统                       | ");
		System.out.println("         |***************************|");
		System.out.println("小主，请选择：");
		int flag=1;
		while(true) {
		switch(in.nextInt()) {
		case 1:
			System.out.println("小主，您的购物清单为：");
			cart.Show();
			System.out.println("总计："+cart.Sum());
			System.out.println("您还想继续什么操作？请选择：");
			break;
		case 2:
			System.out.println("小主，请问您要删除第几条商品");
			int choice=in.nextInt();
			cart.Remove(choice);
			System.out.println("您当前购物清单为：");
			cart.Show();
			System.out.println("您还想继续什么操作？请选择：");
			break;
		case 3:
			System.out.println("小主，您要更改第几条商品件数并且更改为几件？");
			cart.ChangeAmount(in.nextInt(), in.nextInt());
			System.out.println("您当前购物清单为：");
			cart.Show();
			System.out.println("您还想继续什么操作？请选择：");
			break;
		case 4:
			cart.Delete();
			System.out.println("您已成功清空购物车");
			System.out.println("您还想继续什么操作？请选择：");
			break;
		case 5:
			return;
		default:
			System.out.println("请输入正确的编号：（1-5）");
			break;
		}
	
		}
	}

}

