package Shopping;

public interface ProductDao{  //ProductDao接口
	public abstract boolean Add(Product p);  //添加操作
	public abstract void Show();  //展示购物清单
	public abstract void Remove(int i);  //移除商品
	public abstract void ChangeAmount(int i,int Amount);  //改变数量
	public abstract void Delete();   //清空购物车
	public abstract String Sum();   //计算总价
}
