package ptaweek10;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class User implements Comparable<User> {
	private int age;

	public User(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	@Override
	public int compareTo(User o) {

		return age - o.age;
	}

	@Override
	public String toString() {
		return "User [age=" + age + "]";
	}

}

class StuUser extends User {
	private String no;

	public StuUser(int age, String no) {
		super(age);
		this.no = no;
	}

	public String getNo() {
		return no;
	}

	@Override
	public String toString() {
		return "StuUser [no=" + no + ", toString()=" + super.toString() + "]";
	}

}

class UserReverseComparator implements Comparator<User> {

	@Override
	public int compare(User o1, User o2) {
		return o2.getAge() - o1.getAge();   //降序排列
	}

}

class StuUserComparator implements Comparator<StuUser> {

	@Override
	public int compare(StuUser o1, StuUser o2) {
		return o1.getNo().compareTo(o2.getNo());  //升序排列
	}

}

public class GenericMain {

	public static void main(String[] args) {
		
		/*  List<String> strList=new ArrayList<String>();
		  List<Integer> intList=new ArrayList<Integer>();
		  strList.add("a");
		  strList.add("b");
		  strList.add("c");
		  intList.add(1);
		  intList.add(2);
		  intList.add(3);
		  System.out.println(max(strList));
		  System.out.println(max(intList));*/
	/*	    List<StuUser> stuList = new ArrayList<StuUser>();
	        stuList.add(new StuUser(1, "a"));
	        stuList.add(new StuUser(2, "b"));
	        stuList.add(new StuUser(3, "c"));
	        stuList.add(new StuUser(3, "e"));
	        stuList.add(new StuUser(4, "d"));
	        User user1 = max1(stuList,new StuUserComparator());
	        Object user2 = max1(stuList,new StuUserComparator());
	        System.out.println(user1);
	        System.out.println(user2);
		*/
			User user1 = new User(19);
	        User user2 = new User(20);
	        int a = myCompare(user1,user2,new UserReverseComparator());   
	        if(a>0)
	            System.out.println("user1: "+user1+" < "+"user2: "+user2);
	        else if(a<0)
	        	System.out.println("user1: "+user1+" > "+"user2: "+user2);
	        else
	        	System.out.println("user1: "+user1+" = "+"user2: "+user2);
	        StuUser stu1 = new StuUser(1, "a");
	        StuUser stu2 = new StuUser(2, "b");
	        int b = myCompare(stu1,stu2,new StuUserComparator());    
	        if(b>0)
	        	System.out.println("stu1: "+stu1+" > "+"stu2: "+stu2);
	        else if(b<0)
	        	System.out.println("stu1: "+stu1+" < "+"stu2: "+stu2);
	        else
	        	System.out.println("stu1: "+stu1+" = "+"stu2: "+stu2);
	}

	  
/*	  public static <T extends Comparable<T>> T max(List<T> list){
	        Collections.sort(list);
	        return list.get(list.size()-1); 
	    }
	  */
	/*   public static <T extends Comparable< ? super T>> T max1(List<T> list,Comparator< ? super T> comp) {
	        
	        Collections.sort(list,comp);
	        return list.get(list.size()-1);
	      }*/
	  public static <T extends User> int myCompare(T o1,T o2, Comparator<? super T> c){
	        return c.compare(o1, o2);
	}
}