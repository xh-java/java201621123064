package ptaweek10;


import java.util.ArrayList;
import java.util.Scanner;

class ArrayListGeneralStack<T> implements GeneralStack<T>{
	ArrayList<T> list=new ArrayList<T>();
	
	
	public T push(T item) {
		if(item==null)
			return null;
		else{
			list.add(item);
			return item;
		}
	}

	
	public T pop() {
		if(list.size()==0)
			return null;
		else{
			return list.remove(list.size()-1);
		}
	}

	
	public T peek() {
		if(list.size()==0)
			return null;
		else{
			return list.get(list.size()-1);
		}
	}

	
	public boolean empty() {
		if(list.size()==0)
			return true;
		else
			return false;
	}

	
	public int size() {
		return list.size();
	}
	
	
	

	
	
	public String toString() {
		return list.toString();
	}
	
}


class Car{
	private int id;
	private String name;
	

	
	
	/*public Car(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}*/

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	
	
	
}

/*public class Main5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		while(!s.equals("quit")){
			switch (s) {
			case "Integer":
				int sum=0;
				int m = sc.nextInt();
				int n = sc.nextInt();
				ArrayListGeneralStack<Integer> stack = new ArrayListGeneralStack<Integer>();
				System.out.println("Integer Test");
				for(int i=0;i<m;i++){
					int a = sc.nextInt();
					stack.push(a);
					System.out.println("push:"+a);
				}
				for(int i=0;i<n;i++){
					System.out.println("pop:"+stack.pop());
				}
				
				System.out.println(stack.toString());
				while(!stack.empty()){
					sum+=stack.pop();
				}
				System.out.println("sum="+sum);
				System.out.println(stack.getClass().getInterfaces()[0]);
				s=sc.next();
				break;
			case "Double":
				double sum1=0;
				int m1 = sc.nextInt();
				int n1 = sc.nextInt();
				ArrayListGeneralStack<Double> stack1 = new ArrayListGeneralStack<Double>();
				System.out.println("Double Test");
				for(int i=0;i<m1;i++){
					double a = sc.nextDouble(); 
					stack1.push(a);
					System.out.println("push:"+a);
				}
				for(int i=0;i<n1;i++){
					System.out.println("pop:"+stack1.pop());
				}
				System.out.println(stack1.toString());
				while(!stack1.empty()){
					sum1+=stack1.pop();
				}
				System.out.println("sum="+sum1);
				System.out.println(stack1.getClass().getInterfaces()[0]);
				s=sc.next();
				break;

			case "Car":
				int m2 = sc.nextInt();
				int n2 = sc.nextInt();
				ArrayListGeneralStack<Car> stack2 = new ArrayListGeneralStack<Car>();
				System.out.println("Car Test");
				
				for(int i=0;i<m2;i++){
					Car car = new Car();
					car.setId(sc.nextInt());
					car.setName(sc.next());

					stack2.push(car);
					System.out.println("push:"+car);
				}
				for(int i=0;i<n2;i++){
					System.out.println("pop:"+stack2.pop());
				}
				System.out.println(stack2.toString());
				while(!stack2.empty()){
					System.out.println(stack2.pop().getName());
				}
				System.out.println(stack2.getClass().getInterfaces()[0]);
				s=sc.next();
				break;
			}
		}

	}

}*/
public class  Main5{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		String s=sc.next();
		while(!s.equals("quit")){
			switch(s){
			case "Integer":
				int m=sc.nextInt();
				int n=sc.nextInt();
				int sum=0;
				ArrayListGeneralStack<Integer> stack =new ArrayListGeneralStack<Integer>();
				System.out.println("Integer Test");
				for(int i=0;i<m;i++){
					Integer b=sc.nextInt();
					System.out.println("push:"+stack.push(b));
				}
				for(int j=0;j<n;j++){
					Integer a=stack.pop();
					System.out.println("pop:"+a);
				}
				System.out.println(stack);
				while(stack.empty()!=true){
					int c=stack.pop();
					sum=sum+c;
				}
				System.out.println("sum="+sum);
				System.out.println(stack.getClass().getInterfaces()[0]);
				s=sc.next();
				break;
				
			case "Double":
				int p=sc.nextInt();
				int q=sc.nextInt();
				double sum2=0;
				ArrayListGeneralStack<Double> stack2 =new ArrayListGeneralStack<Double>();
				System.out.println("Double Test");
				for(int i=0;i<p;i++){
					Double b=sc.nextDouble();
					System.out.println("push:"+stack2.push(b));
				}
				for(int j=0;j<q;j++){
					Double a=stack2.pop();
					System.out.println("pop:"+a);
				}
				System.out.println(stack2);
				while(stack2.empty()!=true){
					double c=stack2.pop();
					sum2=sum2+c;
				}
				System.out.println("sum="+sum2);
				System.out.println(stack2.getClass().getInterfaces()[0]);
				s=sc.next();
				break;
			case "Car":
				int c=sc.nextInt();
				int d=sc.nextInt();
				ArrayListGeneralStack<Car> stack3 =new ArrayListGeneralStack<Car>();
				System.out.println("Car Test");
				for(int i=0;i<c;i++){
					Car car = new Car();
					car.setId(sc.nextInt());
					car.setName(sc.next());
					stack3.push(car);
					System.out.println("push:"+car);
				}
				for(int j=0;j<d;j++){
					Car car2=stack3.pop();
					System.out.println("pop:"+car2);
				}
				System.out.println(stack3);
				while(stack3.empty()!=true){
					Car car3=stack3.pop();
					System.out.println(car3.getName());
				}
				System.out.println(stack3.getClass().getInterfaces()[0]);
				s=sc.next();
				break;
			}
		}
	}

}


