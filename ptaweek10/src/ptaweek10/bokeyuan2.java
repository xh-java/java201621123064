package ptaweek10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


class Student2 {
	private Long id;
	private String name;
	private int age;
	private Gender gender;// 枚举类型
	private boolean joinsACM; // 是否参加过ACM比赛

	public Student2(Long id, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public Gender getGender() {
		return gender;
	}

	public boolean isJoinsACM() {
		return joinsACM;
	}

	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM=" + joinsACM
				+ "]";
	}

}

public class bokeyuan2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("xiehan 201621123064");
		List<Student2> s = new ArrayList<Student2>();
		s.add(null);
		s.add(new Student2(1L, "zhao", 18, Gender.boy, true));
		s.add(new Student2(2L, "xie", 18, Gender.boy, true));
		s.add(new Student2(3L, "han", 19, Gender.girl, true));
		s.add(null);
		s.add(null);
		s.add(new Student2(4L, "lin", 17, Gender.girl, false));
		s.add(new Student2(5L, "xie", 19, Gender.girl, false));
		s.add(new Student2(6L, "jing", 17, Gender.girl, true));
		List<Student2> newStudent=s.stream().filter(e->e!=null&&e.getId()>1L&&e.getName().equals("xie")&&e.getAge()>17&&e.getGender()==Gender.girl&&e.isJoinsACM()==false).collect(Collectors.toList());
		for (Student2 s2 : newStudent) {
			System.out.println(s2.toString());
		}
	}

}
