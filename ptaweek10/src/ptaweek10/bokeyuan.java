package ptaweek10;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Student{
	private Long id;
	private String name;
	private int age;
	private Gender gender;//枚举类型
	private boolean joinsACM; //是否参加过ACM比赛
	
	public Student(Long id, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
	}
	
	
	
	public Long getId() {
		return id;
	}


	public String getName() {
		return name;
	}


	public int getAge() {
		return age;
	}



	public Gender getGender() {
		return gender;
	}



	public boolean isJoinsACM() {
		return joinsACM;
	}


	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM=" + joinsACM
				+ "]";
	}
	
	
}



public class bokeyuan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("xiehan 201621123064");
		List<Student> s=new ArrayList<Student>();
		s.add(new Student(1L,"zhao",18,Gender.boy,true));
		s.add(new Student(2L,"xie",18,Gender.boy,true));
		s.add(new Student(3L,"wu",19,Gender.girl,true));
		s.add(new Student(4L,"lin",17,Gender.girl,false));
		s.add(new Student(5L,"xie",19,Gender.girl,false));
		s.add(new Student(6L,"jing",17,Gender.girl,true));
		List<Student> newStudent = search(s,1L,"xie",17,Gender.girl,false);
		for (Student s2 : newStudent) {
			System.out.println(s2.toString());
		}
	}

	 private static List<Student> search(List<Student> stuList,Long id, String name, int age, Gender gender, boolean joinsACM){
	        List<Student> student = new ArrayList<Student>();
	        for(Student e:stuList){
	            if(e.getId()>id&&e.getName().equals(name)&&e.getGender()==gender&&e.isJoinsACM()==joinsACM){
	                student.add(e);
	            }
	        }
	        return student;
	        
	    }


}
