package ptaweek10;

public interface GeneralStack<T> {
	T push(T item);           
	T pop();                 
	T peek();               
	public boolean empty();
	public int size();    
}
