package ptaweek12;

import java.util.ArrayList;
import java.util.Scanner;



class Repo{
	private ArrayList arr=new ArrayList();
	private volatile int flag=0;

	public Repo(String items) {
		String[] a=items.split(" ");
		for (int i = 0; i < a.length; i++) {
			arr.add(a[i]);
		}
	}

	public int getSize(){
		return arr.size();
	}
	
	public ArrayList getArr() {
		return arr;
	}

	public int getFlag() {
		return flag;
	}

	public void setArr(ArrayList arr) {
		this.arr = arr;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}
	
	public synchronized void task1(Repo r1){
		if(r1.getSize()>0){
			while(flag==1){
				try{
					wait();
				}catch(InterruptedException e){
					System.out.println(e);
				}
			}
			System.out.println(Thread.currentThread().getName()+" finish "+r1.getArr().get(0));
			r1.getArr().remove(0);
			r1.setFlag(1);
			notifyAll();
		}
	}
	
	public synchronized void task2(Repo r2){
		if(r2.getSize()>0){
			while(flag==0){
				try{
					wait();
				}catch(InterruptedException e){
					System.out.println(e);
				}
			  }
				System.out.println(Thread.currentThread().getName()+" finish "+r2.getArr().get(0));
				r2.getArr().remove(0);
				r2.setFlag(0);
				notifyAll();
				
		}
	}
	
	
}

class Worker1 implements Runnable{
	private Repo repo;
	private int length;
	
	public Worker1(Repo repo) {
		this.repo = repo;
		length=repo.getSize();
	}

	public synchronized void run() {
		for(int i=0;i<length;i++){
			repo.task1(repo);
		}
		
	}
	
	
}

class Worker2 implements Runnable{
	private Repo repo;
	private int length;

	public Worker2(Repo repo) {
		this.repo = repo;
		length=repo.getSize();
	}

	public synchronized void run() {
		for(int i=0;i<length;i++){
			repo.task2(repo);
		}
		
	}
	
}

public class Main6_6 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        Repo repo = new Repo(sc.nextLine());
        Thread t1 = new Thread(new Worker1(repo));  //同一个仓库
        Thread t2 = new Thread(new Worker2(repo));  //同一个仓库
        t1.start();
        Thread.yield();
        t2.start();
        sc.close();
    }
}