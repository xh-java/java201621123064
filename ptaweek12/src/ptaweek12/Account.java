package ptaweek12;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Account{  //谢晗  201621123064
	private int balance;
	
	public Account(int balance) {
		this.balance = balance;
	}
	public int getBalance() {
		return balance;
	}
	
	public void deposit(int money){
		synchronized (this) {
			this.balance=this.balance+money;
		}
	}
	public void withdraw(int money) throws IllegalStateException{
		synchronized (this) {
			this.balance=this.balance-money;
			if(balance<0) 
		        throw new IllegalStateException(balance+"");	
		    }
		}
}