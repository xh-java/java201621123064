package ptaweek12;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account3 {  //谢晗 201621123064
	private int balance;
	private Lock lock=new ReentrantLock();
	private Condition condition=lock.newCondition();
	
	public Account3(int balance) {
		this.balance = balance;
	}
	
	public int getBalance() {
		return balance;
	}
	
	public void deposit(int money){  
		lock.lock();
		try {
			this.balance=this.balance+money;
			condition.signalAll();
		} finally {
			lock.unlock();
		}
	}
	public void withdraw(int money){
		lock.lock();
		try {
			while(this.balance-money<0){
				try{
					condition.await();
				}catch(InterruptedException e){
					System.out.println(e);				
				}
			}
			this.balance=this.balance-money;
			condition.signalAll();
		} finally {
			lock.unlock();
		}
	}

}