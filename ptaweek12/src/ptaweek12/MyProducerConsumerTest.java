package ptaweek12;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;



public class MyProducerConsumerTest {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		System.out.println("谢晗 20621123064");
		Repository repo = new Repository();    //同一个仓库
		Thread producer = new Thread(new Producer(repo,100));//放入100个
		Thread consumer = new Thread(new Consumer(repo,100));//取出100个
		producer.start();
		consumer.start();
		producer.join();
		consumer.join();
		
		System.out.format("main end!仓库还剩%d个货物%n",repo.size());
	}

}

class Repository {// 存放字符串的仓库
	private Lock lock=new ReentrantLock();
	private Condition condition=lock.newCondition();
	private int capacity = 10;//仓库容量默认为10
	private List<String> repo = new ArrayList<String>();// repo(仓库)，最多只能放10个

	public void add(String t) {
		lock.lock();
		try {
			while(repo.size() >= capacity) {
				try{
					condition.await();
				}catch(InterruptedException  e){
					System.out.println(e);
				}
			}
				repo.add(t);
				condition.signalAll();
		} finally {
			lock.unlock();
		}
	}
	public void remove() {
		lock.lock();
		try {
			while(repo.size() <= 0) {
				try{
					condition.await();
				}catch(InterruptedException  e){
					System.out.println(e);
				}
			}
				repo.remove(0);
				condition.signalAll();
		} finally {
			lock.unlock();
		}		
	}
	public synchronized int size(){
		return repo.size();
	}
}

class Producer implements Runnable {

	private Repository repo;
	private int count;//让Producer放入count次

	public Producer(Repository repo,int count) {
		this.repo = repo;
		this.count = count;
	}

	@Override
	public void run() {
		for (int i = 0; i < count; i++) {
			repo.add(new String("sth"));// 每回都放入一个新的货物(字符串对象)
		}
		System.out.format("放入%d个货物完毕!%n",count);
	}

}

class Consumer implements Runnable {
	private Repository repo;
	private int count;//让Consumer取count次

	public Consumer(Repository repo,int count) {
		this.repo = repo;
		this.count = count;
	}

	@Override
	public void run() {
		for (int i = 0; i < count; i++) {
			repo.remove();//每回都从仓库中取出一个货物
		}
		System.out.format("取出%d个货物完毕!%n",count);
	}

}
