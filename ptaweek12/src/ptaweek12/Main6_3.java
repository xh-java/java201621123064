package ptaweek12;

import java.util.Arrays;

public class Main6_3 {	
    public static void main(String[] args) {
        final String mainThreadName = Thread.currentThread().getName();
        /*Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println(mainThreadName);
				System.out.println(Thread.currentThread().getName());
				System.out.println(Arrays.toString(getClass().getInterfaces()));
			}
		});*/
        Thread t2=new Thread(()->{
        	System.out.println(mainThreadName);
			System.out.println(Thread.currentThread().getName());
			System.out.println(Arrays.toString(Thread.class.getInterfaces()));
        });
        
        t2.start();
    }
}
