package ptaweek12;

public class Account1 {
	private int balance;
	
	public Account1(int balance) {
		this.balance = balance;
	}
	
	public int getBalance() {
		return balance;
	}
	
	public synchronized void deposit(int money){
		this.balance=this.balance+money;
	}
	
	
	public synchronized void withdraw(int money) throws IllegalStateException{
		this.balance=this.balance-money;
		if(balance<0) 
	        throw new IllegalStateException(balance+"");	
	    }
}
	
