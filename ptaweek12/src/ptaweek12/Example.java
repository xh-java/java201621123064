package ptaweek12;

import java.util.Scanner;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Scanner;
class Repo1{

	private ArrayList<String> task = new ArrayList<String>();
	private volatile int flag = 0;
	
	public Repo1(String items) {
		String[] t = items.split(" ");
		for(int i=0;i<t.length;i++){
			task.add(t[i]);
			//System.out.println("..");
			//System.out.println(t[i]);
		}
		//for(int i=0;i<t.length;i++){
			//System.out.println(task.get(i));
		//}
		
	}
	public int getSize(){
		return task.size();
	}
	public ArrayList getTask() {
		return task;
	}
	public void setTask(ArrayList task) {
		this.task = task;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public synchronized void task1() {
		if(getSize()>0){
			while(flag==1){
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
			System.out.println(Thread.currentThread().getName()+" finish "+getTask().get(0));
			getTask().remove(0);
			setFlag(1);
			notifyAll();
		}
		
	}
	public synchronized void task2() {
		if(getSize()>0){
			while(flag==0){
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println(Thread.currentThread().getName()+" finish "+getTask().get(0));
			getTask().remove(0);
			setFlag(0);
			notifyAll();
		}
		
	}
	
	
}

class Worker3 implements Runnable{
	private Repo1 repo;
	private int length;
	public Worker3(Repo1 repo){
		this.repo = repo;
		length = repo.getSize();
	}

	@Override
	public synchronized void run() {
		for(int i=0;i<length;i++)
		repo.task1();
		
		}
		
	}
	
		
	


class Worker4 implements Runnable{
	private Repo1 repo;
	private int length;
	public Worker4(Repo1 repo){
		this.repo = repo;
		length = repo.getSize();
	}

	@Override
	public synchronized void run() {
		for(int i=0;i<length;i++)
		repo.task2();
	}
}


public class Example {
	 public static void main(String[] args) throws InterruptedException {
	        Scanner sc = new Scanner(System.in);
	        Repo1 repo = new Repo1(sc.nextLine());
	        Thread t1 = new Thread(new Worker3(repo));
	        Thread t2 = new Thread(new Worker4(repo));
	        t1.start();
	        Thread.yield();
	        t2.start();
	        sc.close();
	    }
	}