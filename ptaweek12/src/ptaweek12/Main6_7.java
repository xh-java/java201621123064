package ptaweek12;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main6_7 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        ExecutorService exec =Executors.newSingleThreadExecutor();
        for (int i = 0; i < n; i++) {
            exec.execute(new Task(i));
        }
        exec.shutdown();
    }
}