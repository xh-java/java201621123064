package ptaweek9;



import java.util.*;
import java.util.Map.Entry;


public class Example {

	public static void main(String[] args) {
		
		Map<String,String> mp=new TreeMap<String,String>();
		List<String>arr=new ArrayList<String>();
		Scanner sc=new Scanner(System.in);
		
		while(true) {
			String str=sc.nextLine();
			if(str.length()==0)
				continue;
			if(str.equals("!!!!!"))
				break;
			
			arr.add(str);
			String[] strs=str.split(" +");
			for(String s:strs) {
				if(mp.get(s)==null)
					mp.put(s, Integer.toString(arr.size()));
				else
					mp.put(s, mp.get(s)+","+arr.size());
			}					
		}
		for(String s:mp.keySet())
			System.out.println(s+"=["+mp.get(s)+"]");
		
		while(true) {
			
			String str=sc.nextLine();
			if(str==null) 
				continue;
			String[] strs=str.split(" +");
			
			if(strs.length==1) {
				if(mp.get(str)==null) {
					System.out.println("found 0 results");
					continue;
				}
				
				System.out.println("["+mp.get(str)+"]");
				
				String[] str2=mp.get(str).split(",");
				for(int i=0;i<str2.length;i++){
	    			System.out.println(str2[i]);
	    		}
				for(String s:str2)
					System.out.println("line "+Integer.parseInt(s)+":"+(arr.get(Integer.parseInt(s)-1)));
				
				continue;
			}
			
			
			Set<String> se1=new TreeSet<String>();
			Set<String> se2=new TreeSet<String>();
			for(int i=0;i<strs.length-1;i++) {
				
				
				String[] strs1=mp.get( strs[i]).split(",");
				if(strs1.length==0) {
					se1.clear();
					break;
				}
				for(String s:strs1)   
					se1.add(s);
				String[] strs2=mp.get( strs[i+1]).split(",");
				if(strs2.length==0) {
					se1.clear();
					break;
				}
				for(String s:strs2)
					se2.add(s);
				
				se1.retainAll(se2); 
				
			}
			
			if(se1.isEmpty()) {
				System.out.println("found 0 results");
				continue;
			}
			System.out.println(se1);
			
			for(String s:se1)
				System.out.println("line "+Integer.parseInt(s)+":"+ (arr.get(Integer.parseInt(s)-1)));
			
		}			
		
	}

}
