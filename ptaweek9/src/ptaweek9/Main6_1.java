package ptaweek9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main6_1 {

    /*covnertStringToList函数代码*/   
		
    /*remove函数代码*/
		
     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            List<String> list = convertStringToList(sc.nextLine());
            System.out.println(list);
            String word = sc.nextLine();
            remove(list,word);
            System.out.println(list);
        }
        sc.close();
    }

	private static List<String> convertStringToList(String nextLine) {
		// TODO Auto-generated method stub
		List<String> list=new ArrayList<String>();
		String[] str=nextLine.split("\\s+");
	    for(int i=0;i<str.length;i++){
	    	list.add(str[i]);
	    }
		return list;
	}

	private static void remove(List<String> list, String word) {
		// TODO Auto-generated method stub
		for(int i=list.size()-1;i>=0;i--){
			if(list.get(i).equals(word))
				list.remove(list.get(i));
		}
	}

}