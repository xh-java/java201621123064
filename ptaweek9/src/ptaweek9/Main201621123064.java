package ptaweek9;

import java.util.ArrayList;
import java.util.Scanner;

class Judge implements Stack{

	private ArrayList<Character> list=new ArrayList<>();
	
	public Character push(Character item) {
		if(item==null)
			return null;
		else{
			list.add(item);
			return item;
		}
	}

	public Character pop() {
		if(list.size()==0)
			return null;
		else{
			Character a=list.get(list.size()-1);
			list.remove(list.size()-1);
			return a;
		}
	}
	
	public Character peek() {
		if(list.size()==0)
			return null;
		else{
			return list.get(list.size()-1);
		}
	}
	
	public boolean empty() {
		if(list.size()==0)
			return true;
		else
			return false;
	}

	public int size() {
		return list.size();
	}
	
}

public class Main201621123064 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner sc=new Scanner(System.in);
	while(sc.hasNext()){
		Judge judge=new Judge();
		String a=sc.nextLine();
		char[] str=a.toCharArray();
		for(int i=0;i<str.length;i++){
			judge.push(str[i]);
		}
		if(judge.empty()==true)
			System.out.println("不是回文");
		else{
			for(int j=0;j<str.length;j++){
				if(str[j]!=judge.peek()){
					break;
				}
				else{
					judge.pop();
				}
			}
			if(judge.empty()==false)
				System.out.println("不是回文");
			else
				System.out.println("是回文");
		}
		
	}
	}
}
