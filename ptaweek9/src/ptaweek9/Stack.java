package ptaweek9;

public interface Stack {
	public Character push(Character item);
	
	public Character pop();  
	public Character peek();  
	public boolean empty(); 
	public int size();      
}
