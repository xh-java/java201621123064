package ptaweek9;

import java.util.ArrayList;
import java.util.Scanner;

class ArrayListIntegerStack implements IntegerStack{
	private ArrayList<Integer> list=new ArrayList<>();
	
	
	
	public Integer push(Integer item){
		if(item==null)
			return null;
		else{
			list.add(item);
			return item;
		}
		
	}
	
	public Integer pop(){
		if(list.size()==0)
			return null;
		else{
			int a=list.get(list.size()-1);
			list.remove(list.size()-1);
			return a;
		}
		
	}
	
	public Integer peek(){
		if(list.size()==0)
			return null;
		else{
			return list.get(list.size()-1);
		}
	}
	
	public boolean empty(){
		if(list.size()==0)
			return true;
		else
			return false;
	}
	
	public int size(){
		return list.size();
	}

	
	public String toString() {
		return list.toString();
	}
	
	
	
	
	
	
}

public class Main1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayListIntegerStack arr=new ArrayListIntegerStack();
		Scanner sc=new Scanner(System.in);
		int m=sc.nextInt();
		for(int i=0;i<m;i++){
			Integer a=arr.push(sc.nextInt());
			System.out.println(a);
		}
		System.out.println(arr.peek()+","+arr.empty()+","+arr.size());
		System.out.println(arr);
		int x=sc.nextInt();
		for(int i=0;i<x;i++){
			Integer b=arr.pop();
			System.out.println(b);
		}
		System.out.println(arr.peek()+","+arr.empty()+","+arr.size());
		System.out.println(arr);
	}

}
