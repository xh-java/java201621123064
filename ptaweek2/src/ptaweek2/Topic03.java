package ptaweek2;

import java.util.Arrays;
import java.util.Scanner;

public class Topic03 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
    while(true){
    	int n=Integer.parseInt(sc.nextLine());
    	String[][] str=print(n);
    	System.out.println(Arrays.deepToString(str));
    }
	}

	private static String[][] print(int n) {
		String[][] str=new String[n][];
	  for(int i=0;i<n;i++){
		  str[i]=new String[i+1];
		  for(int j=0;j<=i;j++){
			  str[i][j]=(i+1)+"*"+(j+1)+"="+(i+1)*(j+1);
			  if(j==i)
					System.out.printf("%s"+"\n",str[i][j]);
				else
					System.out.printf("%-7s",str[i][j]);
			  }
	  }
	  return str;
	}

}
