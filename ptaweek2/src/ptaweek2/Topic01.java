package ptaweek2;

import java.util.Scanner;

public class Topic01 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);		
		while (true) {
			StringBuilder stringBuilder = new StringBuilder();
			int n = sc.nextInt();
			for(int i=0;i<n;i++){
				stringBuilder.append(i);
			}
			int x = sc.nextInt();
			int y = sc.nextInt();
			System.out.println(stringBuilder.subSequence(x, y));

		}
	}

}