package ptaweek3;

import java.util.ArrayList;
import java.util.Scanner;

abstract class Goods{   //商品类
	private double price;
	private String name;
	private int amount;
	public abstract double getPrice();
	public abstract String getName();
	public abstract int  getAmount();
	public abstract void  setAmount(int amount);
}

class Books extends Goods{   //书籍类
	private double price;
	private String name;
	private int amount;
	
	public double getPrice() {
		return price;
	}
	public String getName() {
		return name;
	}
	public int  getAmount() {
		return this.amount;
	}
	public void setAmount(int amount) {
		this.amount=amount;
	}
	public Books(double price, String name) {
		this.price = price;
		this.name = name;
	}
	
	public Books(String name, int amount) {
		this.name = name;
		this.amount = amount;
	}
	public Boolean Add(int amount) {   //添加到购物车
		return true;
	}
	public Boolean Collect(String name) {   //收藏起来
		return true;
	}
	

}
class ChildrenBook extends Books{  //儿童书类继承自书籍类
	public ChildrenBook(double price, String name) {
		super(price,name);
	}

}
class TextBook extends Books{   //教科书类继承自书籍类
	public TextBook(double price, String name) {
		super(price,name);
	}

}
class OutsideReading extends Books{   //课外读物类继承自书籍类
	public OutsideReading(double price, String name) {
		super(price,name);
	}
}

class Snakes extends Goods{   //零食类
	private double price;
	private String name;
	private int amount;
	public double getPrice() {
		return price;
	}
	public String getName() {
		return name;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getAmount() {
		return this.amount;
	}
	public Snakes(double price, String name) {
		this.price = price;
		this.name = name;
	}
	
	public Snakes(String name, int amount) {
		this.name = name;
		this.amount = amount;
	}
	public Boolean Add(int amount) {   //添加到购物车
		return true;
	}
	
	public Boolean Collect(String name) {   //收藏起来
		return true;
	}
}
class Nut extends Snakes{   //坚果类继承自零食类
	public Nut(double price, String name) {
		super(price,name);
	}
}

class Chips extends Snakes{   //薯片类继承自零食类
	public Chips(double price, String name) {
		super(price,name);
	}
}

class Bread extends Snakes{   //面包类继承自零食类
	public Bread(double price, String name) {
		super(price,name);
	}
}


class Shoppingcart{    //购物车类
	private double sum=0;
	private Goods []good;
	
	public double AllSum(Goods []goods) {  //计算购物车中所有商品的总价
		double sum=0.0;
		for(int i=0;i<goods.length;i++) 
			sum+=goods[i].getPrice()*goods[i].getAmount();
		return sum;
	}
	
	public boolean Delete(ArrayList<Goods> goods) {   //删除购物车中的物品
		return true;
	}
}
class User{
	private String username;
	private String userpassport;
	
	public User(String username, String userpassport) {
		this.username = username;
		this.userpassport = userpassport;
	}
}
public class taobao {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in =new Scanner(System.in);
		Goods []good=new Goods[2];
		good[0]=new Books(5, "Java");
		good[1]=new Snakes(3,"abc");
		ArrayList good1=new ArrayList<Goods>();
		System.out.println("Hello！Can I help you ?There are books and snakes.What do you want?");
		String str=in.next();
		while(!str.equals("exit")) {
			if(str.equals("book")) {
				System.out.println("Which book do you want?And how many?");
				String str1=in.next();
				for(int i=0;i<2;i++) {
					if(str1.equals(good[i].getName())) {
						Goods goods=new Books(str1, in.nextInt());
						goods.setAmount(good[i].getAmount());
						good1.add(goods);
						break;
					}
					else continue;
				}
			}
			else if(str.equals("snake")) {
				System.out.println("Which book do you want?And how many?");
				String str1=in.next();
				for(int i=0;i<2;i++) {
					if(str1.equals(good[i].getName())) {
						Goods goods=new Snakes(str1, in.nextInt());
						goods.setAmount(good[i].getAmount());
						good1.add(goods);
						break;
					}
					else continue;
				}
			System.out.println("Are you ok?");
			switch(in.next()) {
			case"yes":break;
			case"no":continue;
			}
			str=in.next();
			}
		}

	}

}
