package ptaweek3;

import java.util.Arrays;
import java.util.Scanner;




class PersonOverride{
	private String name;
	private int age;
	private boolean gender;
	
	public PersonOverride(){
		this("default",1,true);
	}
	
	
	public PersonOverride(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	
	public String toString(){
		return name+"-"+age+"-"+gender;
	}
	
	
	public boolean equals(PersonOverride s){
		return (this.equals(s));
	}
	
	
	
	
/*	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonOverride other = (PersonOverride) obj;
		if (age != other.age)
			return false;
		if (gender != other.gender)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}*/
	
	
	
}



public class Main2 {
	public static void main(String[] args) {
	    PersonOverride[] persons1=new PersonOverride[100];
	    PersonOverride[] persons2=new PersonOverride[100];
	    Scanner sc=new Scanner(System.in);
	    int k=0;
	    int n1=Integer.parseInt(sc.nextLine());
	    for(int i=0;i<n1;i++){
	    	PersonOverride person=new PersonOverride();
	    	persons1[i]=person;
	    }
	    
	    int n2=Integer.parseInt(sc.nextLine());
	    for(int i=0;i<n2;i++){
	    	PersonOverride person1=new PersonOverride(sc.next(),sc.nextInt(),sc.nextBoolean());
	    	if(i==0)
				persons2[k++] = person1;
			
			if(i>0){
				for(int j=0;j<k;j++){
					if(persons2[j].toString().equals(person1.toString()))
						break;
					if(j==k-1){
						persons2[k++]=person1;
					}
				}
				
			}
	    }
	    
	    
	    for(int i=0;i<n1;i++){
	    	System.out.println(persons1[i]);
	    }
	    for(int i=0;i<k;i++){
	    	System.out.println(persons2[i]);
	    }
	    System.out.println(k);
	    System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
		}

	}

