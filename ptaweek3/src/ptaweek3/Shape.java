package ptaweek3;

import java.util.Arrays;
import java.util.Scanner;

class Rectangle 
{
	private int width;
	private int length;
	public Rectangle(int width,int length){
		this.length=length;
		this.width=width;
	}
	public int getPerimeter(){
		return (2*(width+length));
	}
	public int getArea(){
		return (width*length);
	}
	public String toString(){
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
	
}

class Circle 
{
	private int radius;
	public Circle(int radius){
		this.radius=radius; 
	}
	public int getPerimeter(){
		double a=2*Math.PI*radius;
		int b=(int)a;
		return b;
	}
	public int getArea(){
		double a=Math.PI*radius*radius;
		int b=(int)a;
		return b;
	}
	public String toString(){
		return "Circle [radius=" + radius + "]";
	}
	
}


public class Shape {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Scanner sc=new Scanner(System.in);
    Rectangle[] a=new Rectangle[2];
    Circle[] b=new Circle[2];
    int sum=0;
    for(int i=0;i<2;i++){
    	Rectangle c=new Rectangle(sc.nextInt(),sc.nextInt());
    	a[i]=c;
    }
    for(int i=0;i<2;i++){
    	Circle d=new Circle(sc.nextInt());
    	b[i]=d;
    }
    for(int j=0;j<2;j++){
    	int m=a[j].getPerimeter()+b[j].getPerimeter();
    	sum=sum+m;
    }
    System.out.println(sum);
    sum=0;
    for(int j=0;j<2;j++){
    	int m=a[j].getArea()+b[j].getArea();
    	sum=sum+m;
    }
    System.out.println(sum);
    System.out.println(Arrays.deepToString(a));
    System.out.println(Arrays.deepToString(b));
	}

}
