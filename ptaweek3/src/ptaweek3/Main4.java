package ptaweek3;


import java.text.DecimalFormat;
import java.util.Scanner;

class Person02{
	private String name;
	private int age;
	private boolean gender;
	public Person02(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	@Override
	public String toString() {
		return name+"-"+age+"-"+gender;
	}
	
	
}
class Company{
	private String Logo;

	public Company(String logo) {
		Logo = logo;
	}

	@Override
	public String toString() {
		return Logo;
	}
	
}
class Employee extends Person{
	private Company company;
	private double salary;
	public Employee(String name, int age, boolean gender, Company company, double salary) {
		super(name, age, gender);
		this.company = company;
		this.salary = salary;
	}

	public String toString() {
		return super.toString()+"-"+company+"-"+salary;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		DecimalFormat df = new DecimalFormat("#.##");
		String a = df.format(this.salary);
		double a1 = Double.parseDouble(a);
		String b = df.format(other.salary);
		double b1 = Double.parseDouble(b);
		if (a1 != b1)
			return false;
		return true;
	}

}
public class Main4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		Company str1=new Company(in.next());
		Employee employ=new Employee(in.next(), in.nextInt(), in.nextBoolean(), str1, in.nextDouble());
		System.out.println(employ);
	}

}
