package ptaweek3;

import java.util.Scanner;

class Rational {
	private int v1;
	private int v2;
	public void Setv1(int v1){
		this.v1=v1;
	}
	public int Getv1(){
		return v1;
	}
	public void Setv2(int v2){
		this.v2=v2;
	}
	public int Getv2(){
		return v2;
	}
	public Rational(int v1,int v2){
		this.v1=v1;
		this.v2=v2;
	}
	public Rational(){
		
	}
	public static void Add(Rational T1,Rational T2,Rational T3){
		int t;
		T3.Setv1(T1.Getv1()*T2.Getv2()+T2.Getv1()*T1.Getv2());
		T3.Setv2(T1.Getv2()*T2.Getv2());
		t=Gcd(T3.Getv1(),T3.Getv2());
		T3.Setv1(T3.Getv1()/t);
        T3.Setv2(T3.Getv2()/t);
	}
	
	public static void Minus(Rational T1,Rational T2,Rational T3){
		int t;
		T3.Setv1(T1.Getv1()*T2.Getv2()-T2.Getv1()*T1.Getv2());
		T3.Setv2(T1.Getv2()*T2.Getv2());
		t=Gcd(T3.Getv1(),T3.Getv2());
		T3.Setv1(T3.Getv1()/t);
        T3.Setv2(T3.Getv2()/t);
	}
	
	public static void Multiply(Rational T1,Rational T2,Rational T3){
		int t;
		T3.Setv1(T1.Getv1()*T2.Getv1());
		T3.Setv2(T1.Getv2()*T2.Getv2());
		t=Gcd(T3.Getv1(),T3.Getv2());
		T3.Setv1(T3.Getv1()/t);
        T3.Setv2(T3.Getv2()/t);
	}
	public static void Divide(Rational T1,Rational T2,Rational T3){
		int t;
		T3.Setv1(T1.Getv1()*T2.Getv2());
		T3.Setv2(T1.Getv2()*T2.Getv1());
		t=Gcd(T3.Getv1(),T3.Getv2());
		T3.Setv1(T3.Getv1()/t);
        T3.Setv2(T3.Getv2()/t);
	}


	public static int Gcd(int m, int n) {
	    int t;
	    while(m%n!=0){
	    	t=n;
	    	n=m%n;
	    	m=t;
	    }
	    return n;
	}
	public static int GetRational(Rational T,int i){
		if(i==1)
			return T.Getv1();
		else
			return T.Getv2();
	}
}

//201621123064
//谢晗

public class Main {
    private static Scanner sc;
	public static void main(String[] args) {
		int v1,v2,i,e=0;
	    Rational T1=new Rational();
	    Rational T2=new Rational();
	    Rational T3=new Rational();
	    Scanner sc=new Scanner(System.in);
	    v1=sc.nextInt();
	    v2=sc.nextInt();
        T1=new Rational(v1,v2);
        v1=sc.nextInt();
	    v2=sc.nextInt();
	    T2=new Rational(v1,v2);
	    while(sc.hasNext()){
	    	String a=sc.next();
	    if(a.equals("+")){
	    	 Rational.Add(T1, T2, T3);
	 	     System.out.println(T3.Getv1()+"/"+T3.Getv2());
	    }
	    if(a.equals("-")){
	    	Rational.Minus(T1, T2, T3);
		    System.out.println(T3.Getv1()+"/"+T3.Getv2());
	    }
	    if(a.equals("*")){
	    	Rational.Multiply(T1, T2, T3);
	 	    System.out.println(T3.Getv1()+"/"+T3.Getv2());
	    }
	    if(a.equals("/")){
	    	Rational.Divide(T1, T2, T3);
		    System.out.println(T3.Getv1()+"/"+T3.Getv2());
	    }
	    }
	}

}
