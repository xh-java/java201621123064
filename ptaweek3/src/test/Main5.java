package test;

import java.util.Scanner;



class PersonOverride1{
	private String name;
	private int age;
	private boolean gender;
	
	public PersonOverride1(String name, int age, boolean gender) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public PersonOverride1(){
		this("default",1,true);
	}
	
	public String toString(){
		return "" + name +"-" + age +"-" + gender +"";
	}

	public boolean equals(String p){
		if(this.toString().equals(p))
			return true;
		else
			return false;
	}

	
	
	
	
}
public class Main5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		PersonOverride1 person1=new PersonOverride1(sc.next(),sc.nextInt(),sc.nextBoolean());
		PersonOverride1 person2=new PersonOverride1(sc.next(),sc.nextInt(),sc.nextBoolean());
		System.out.println(person1.toString().equals(person2.toString()));
		}
	   }
