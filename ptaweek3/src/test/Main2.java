package test;

import java.util.Scanner;

class Person01 {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	static int num=0;
	public Person01(){
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%b,%d%n",name,age,gender,id);
		
	}
	public Person01(String name,int age,boolean gender){
		this.name=name;
		this.age=age;
		this.gender=gender;
	}
	
	public String toString(){
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	static{
		System.out.println("This is static initialization block");
	} 
	{
		id=num;
		System.out.printf("This is initialization block, id is %d%n",num++);
	}
}
public class Main2 {

	public static void main(String[] args){
		Scanner in=new Scanner(System.in);
		int n=Integer.parseInt(in.nextLine());
		Person01[] persons=new Person01[n];
		for(int j=0;j<n;j++){
			Person01 person=new Person01(in.next(),in.nextInt(),in.nextBoolean());
			persons[j]=person;
		}
		for(int i=persons.length-1;i>=0;i--){
			System.out.println(persons[i]);
		}
		System.out.println(new Person01());
	}
}
