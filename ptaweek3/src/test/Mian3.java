package test;

import java.util.Arrays;
import java.util.Scanner;
class Shape{
	public static double perimeter=0;
	public static double area=0;
	final double PI = 3.14;
	public double getPerimeter(){
		return 1.0;
	}
	public double getArea(){
		return 1.0;
	}
	public static double sumAllPerimeter(double c){
		perimeter+=c;
		return perimeter;
	}
	public double sumAllArea(double s){
		area+=s;
		return area;
	}
	
	
}
class Rectangle extends Shape{
	private int width,length;
	public Rectangle(int width,int length){
		this.width = width;
		this.length = length;
		
	}

	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
	public double getPerimeter(){
		sumAllPerimeter(width*2+length*2);
		return width*2+length*2;
		
	}
	public double getArea(){
		sumAllArea(width*length);
		return width*length;
	}
	
}

class Circle extends Shape{
	private int radius;
	public Circle(int radius){
		this.radius = radius;
	}
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	public double getPerimeter(){
		sumAllPerimeter(PI*2*radius);
		return PI*2*radius;
		
	}
	public double getArea(){
		sumAllArea(PI*radius*radius);
		return PI*radius*radius;
	}
}



public class Mian3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
	    String[] p = new String[n];
		String[] q= new String[n];
		for(int i=0;i<n;i++){
			String a = sc.next();
			switch (a) {
			case "rect":
				sc.nextLine();
				String a1 = sc.nextLine();
				String[] a2 = a1.split(" ");
				int b = Integer.parseInt(a2[0]);
				int c = Integer.parseInt(a2[1]);
				Rectangle r = new Rectangle(b,c);
				p[i]=r.toString();
				r.getArea();
				r.getPerimeter();
				
				q[i] = r.getClass()+","+r.getClass().getSuperclass();
				break;
			case "cir":
				int e = sc.nextInt();
				Circle y = new Circle(e);
				y.getArea();
				y.getPerimeter();
				p[i] = y.toString();
				q[i] = y.getClass()+","+y.getClass().getSuperclass();
				break;

			}
		}
		Shape s = new Shape();
		System.out.println(s.perimeter);
		System.out.println(s.area);
		System.out.println(Arrays.deepToString(p));
		for(int i=0;i<n;i++){
			System.out.println(q[i]);
		}

	}

}