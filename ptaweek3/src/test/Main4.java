package test;

import java.util.Scanner;

import java.util.Arrays;

class PersonOverride{
	private String name;
	private boolean gender;
	private int age;
	public PersonOverride(){
		this("default",1,true);
	}
	public PersonOverride(String name,int age,boolean gender){
		this.name = name;
		this.age = age;
		this.gender = gender;
	}


	public String toString() {
		return name+"-"+age+"-"+gender;
	}
/*	public boolean equals(String p){
		if(this.toString().equals(p))
			return true;
		else
			return false;
	}
	*/
	public boolean equals(PersonOverride s){
		return (this.equals(s));
	}
}

public class Main4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n1 = sc.nextInt();
		String[] person1 = new String[n1];
		
		for(int i=0;i<n1;i++){
			PersonOverride p = new PersonOverride();
			person1[i] = p.toString();
		}
		int n2 = sc.nextInt();
		int k=0;
		String[] person2 = new String[n2];
		sc.nextLine();
		String[] temp = new String[n2];
		for(int i=0;i<n2;i++){
			temp[i] = sc.nextLine();
		}
		for(int i=0;i<n2;i++){
			String[] p1 = temp[i].split(" ");
			int a = Integer.parseInt(p1[1]);
			PersonOverride p2 = new PersonOverride(p1[0],a,Boolean.valueOf(p1[2]));
			if(i==0)
				person2[k++] = p2.toString();
			
			if(i>0){
				for(int j=0;j<k;j++){
					if(person2[j].equals(p2.toString()))
						break;
					if(j==k-1){
						person2[k++]=p2.toString();
					}
				}
				
			}
		}
		for(int i=0;i<n1;i++){
			System.out.println(person1[i]);
		}
		for(int i=0;i<k;i++){
			System.out.println(person2[i]);
		}
		System.out.println(k);
		System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
		

	}

}
