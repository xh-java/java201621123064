package test;

import java.util.ArrayList;
import java.util.Scanner;

class Fruit{
	private String name;
	public Fruit(String name) {
		super();
		this.name = name;
	}	
	 public String toString() {
	        return "Fruit [name=" + name + "]";
	    }
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruit other = (Fruit) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}
      
	
}
public class Main7 {
	public static void main(String[] args) {
        ArrayList<Fruit> list=new ArrayList<Fruit>();
        Scanner sc=new Scanner(System.in);        
        while(sc.hasNext()){
            String s=sc.nextLine();
            if(s.equals("end"))
                break;
            Fruit fruit=new Fruit(s);
            if(!list.contains(fruit)){
                list.add(fruit);
            }
        }
        System.out.println(list);
}
}





