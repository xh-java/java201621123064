package ptaweek6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable2{
	private String name;
	private int age;
	public PersonSortable2(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public String toString(){
		return this.name+"-"+this.age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
    
	
}

class NameComparator implements Comparator<PersonSortable2>{

	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
    // TODO Auto-generated method stub
        return o1.getName().compareTo(o2.getName());
    }
	
}

class AgeComparator implements Comparator<PersonSortable2>
{
    @Override
    public int compare(PersonSortable2 o1, PersonSortable2 o2) {
        // TODO Auto-generated method stub
        return o1.getAge()-o2.getAge();
    }

}

public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Scanner sc=new Scanner(System.in);
    int n=Integer.parseInt(sc.nextLine());
    ArrayList arr=new ArrayList();
    for(int i=0;i<n;i++){
    	PersonSortable2 a=new PersonSortable2(sc.next(),sc.nextInt());
    	arr.add(a);
    }
    NameComparator namecomparator=new NameComparator();
    Collections.sort(arr, namecomparator);
    System.out.println("NameComparator:sort");
    for(int i=0;i<n;i++){
    	System.out.println(arr.get(i));
    }
    
    AgeComparator agecomparator=new AgeComparator();
    Collections.sort(arr, agecomparator);
    System.out.println("AgeComparator:sort");
    for(int i=0;i<n;i++){
    	System.out.println(arr.get(i));
    }
    
    
    System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
    System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}

}
