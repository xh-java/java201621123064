package ptaweek6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

abstract class Shape implements Comparable<Shape>{

	final double PI=3.14;
	public abstract double getPerimeter();
	public abstract double getArea();
	@Override
	public int compareTo(Shape o) {
		if(this.getPerimeter()-o.getPerimeter()<0)
			return -1;
		else if(this.getPerimeter()-o.getPerimeter()>0)
			return 1;
		else{
			if(this.getArea()-o.getArea()>0)
				return 1;
			else if(this.getArea()-o.getArea()<0)
				return -1;
			else
				return 0;
		}
	}
	
}

 class Rectangle extends Shape{
	private int width;
	private int length;
	
	
	public Rectangle(int width, int length) {
		super();
		this.width = width;
		this.length = length;
	}



	public double getPerimeter(){
		return (width+length)*2;
	}
	
	public double getArea(){
		return width*length;
	}
	
	public String toString(){
		return "recrangle[width = "+ width + ",length = " + length + "]";
	}
}

class Circle extends Shape{
	private int radius;

	public Circle(int radius) {
		super();
		this.radius = radius;
	}
	public double getPerimeter(){
		return 2*PI*this.radius;
	}
	
	public double getArea(){
		return PI*this.radius*this.radius;
	}
	public String toString(){
		return "Circle[radius="+ radius +"]";
	}
}


public class Bokeyuan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Scanner sc=new Scanner(System.in);
    int n=Integer.parseInt(sc.nextLine());
    ArrayList arr=new ArrayList();
    for(int i=0;i<n;i++){
    	String a=sc.nextLine();
    	switch (a) {
		case "rect":
			String a1 = sc.nextLine();
			String[] a2 = a1.split(" ");
			int b = Integer.parseInt(a2[0]);
			int c = Integer.parseInt(a2[1]);
			Rectangle r = new Rectangle(b,c);
			arr.add(r);
			break;
		case "cir":
		    String f=sc.nextLine();
		    int e=Integer.parseInt(f);
			Circle y = new Circle(e);
			arr.add(y);
			break;
    	}
	}
    Collections.sort(arr);
    for(int i=0;i<n;i++){
    	System.out.println(arr.get(i));
    }
	}

}
