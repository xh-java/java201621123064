package ptaweek6;

import java.util.Arrays;
import java.util.Scanner;


class ArrayIntegerStack implements IntegerStack{
	
	private Integer ins[];
	private int top=-1;
	private int max;
	
	public ArrayIntegerStack(int n) {
		ins=new Integer[n];
		max=n;
	}
	
	public Integer[] getIns() {
		return ins;
	}



	public void setIns(Integer[] ins) {
		this.ins = ins;
	}



	public int getMax() {
		return max;
	}
	
	public void setMax(int max) {
		this.max = max;
	}
	
	public int getTop() {
		return top;
	}
	
	public void setTop(int top) {
		this.top = top;
	}
	@Override
	public Integer push(Integer item) {
		// TODO Auto-generated method stub
		if(item==0)
			return null;
		else if(this.top==this.max-1)
			return null;
		else{
			this.top++;
			ins[this.top]=item;
			return item;
		}
	}
	@Override
	public Integer pop() {
		// TODO Auto-generated method stub
		if(this.top==-1)
			return null;
		else{
			this.top--;
			return ins[this.top+1];	
		}
	}
	@Override
	public Integer peek() {
		// TODO Auto-generated method stub
		if(this.top==-1)
			return null;
		else
			return ins[this.top];
	}
	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		if(this.top==-1)
			return true;
		else
			return false;
	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.top+1;
	}
}


public class Main3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		
		int n=Integer.parseInt(sc.nextLine());
		
		ArrayIntegerStack array=new ArrayIntegerStack(n);
		
		int m=Integer.parseInt(sc.nextLine());
		
		for(int i=0;i<m;i++){
			Integer a=new Integer(sc.nextInt());
			Integer b=array.push(a);//入栈
			System.out.println(b);  //输出入栈结果
		}
		
		System.out.println(array.peek()+","+array.empty()+","+array.size());
		System.out.println(Arrays.toString(array.getIns()));
		sc.nextLine();  //吃掉回车
		int x=Integer.parseInt(sc.nextLine());
		for(int i=0;i<x;i++){
			Integer c=array.pop();  //出栈
			System.out.println(c);  //输出出栈结果
		}
		System.out.println(array.peek()+","+array.empty()+","+array.size());
		System.out.println(Arrays.toString(array.getIns()));
	}

}
