package ptaweek6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

class PersonSortable implements Comparable<PersonSortable> {
	private String name;
	private int age;
	public PersonSortable(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public String toString(){
		return this.name+"-"+this.age;
	}

	@Override
	public int compareTo(PersonSortable o) {
		if(name.equals(o.name))
			return this.age-o.age;
		else
			return this.name.compareTo(o.name );
	}
	
}


public class Main1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   Scanner sc=new Scanner(System.in);
   int n=Integer.parseInt(sc.nextLine());
   ArrayList arr=new ArrayList();
   for(int i=0;i<n;i++){
	   PersonSortable a=new PersonSortable(sc.next(),sc.nextInt());
	   arr.add(a);
   }
   Collections.sort(arr);
   for(int i=0;i<n;i++){
	   System.out.println(arr.get(i));
   }
   System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
	}

}
