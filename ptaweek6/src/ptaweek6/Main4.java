package ptaweek6;

import java.util.Arrays;
import java.util.Scanner;

class ArrayUtils{
	static class PairResult {
		private double min;
		private double max;
		
		public String toString() {
			return "PairResult [min=" + min + ", max=" + max + "]";
		}
		
	}
	 static PairResult findMinMax(Double[] values){
	        PairResult pr=new PairResult();
	        Arrays.sort(values);
	        pr.min=values[0];
	        pr.max=values[values.length-1];
	        return pr;
	    }
	
}


public class Main4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		ArrayUtils arr=new ArrayUtils();
		int n=Integer.parseInt(sc.nextLine());
		Double[] a=new Double[n];
		for(int i=0;i<n;i++){
			Double b=new Double(sc.nextDouble());
			a[i]=b;
		}
		
		System.out.println(arr.findMinMax(a));//相当于输出了PairResult的toString方法
		System.out.println(ArrayUtils.PairResult.class);
	}

}
